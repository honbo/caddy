FROM alpine:3.8
RUN apk add --no-cache bash curl openssl 
RUN curl https://getcaddy.com | bash -s personal
EXPOSE 80 443 2015
ENTRYPOINT ["/bin/caddy"]
CMD ["-conf", "/etc/Caddyfile", "-log", "stdout", "-agree=$ACME_AGREE"]